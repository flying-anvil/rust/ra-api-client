# RA-API-Client

_Unofficial_ Rust-Client for the RetroAchievements API, suited for my needs primarily (for now).

Uses the reqwest (async) client as the raw HTTP client. Thus, this client only supports async as of now.

## Docs

See the official API-docs here: <https://api-docs.retroachievements.org/>
