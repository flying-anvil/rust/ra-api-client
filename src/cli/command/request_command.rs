use std::error::Error;

use clap::Subcommand;

use self::user::UserRequestCommand;

mod user;

#[derive(Subcommand, Clone, Debug)]
pub enum RequestCommand {
    #[command(subcommand, name = "user")]
    /// Request things related to the "User" category
    UserRequest(UserRequestCommand),
}

impl RequestCommand {
    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        match self {
            RequestCommand::UserRequest(command) => command.run(),
        }
    }
}
