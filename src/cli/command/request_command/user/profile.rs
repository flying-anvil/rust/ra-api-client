use std::error::Error;

use clap::Args;

#[derive(Args, Clone, Debug)]
pub struct UserProfileRequestCommand {
    /// Username of the subject
    username: String,
}

impl UserProfileRequestCommand {
    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        todo!();

        Ok(())
    }
}
