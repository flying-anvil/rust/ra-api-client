use std::error::Error;

use clap::Subcommand;

use self::profile::UserProfileRequestCommand;

mod profile;

#[derive(Subcommand, Clone, Debug)]
pub enum UserRequestCommand {
    #[command(name = "profile")]
    /// Request the user profile API
    UserProfileRequest(UserProfileRequestCommand),
}

impl UserRequestCommand {
    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        match self {
            UserRequestCommand::UserProfileRequest(command) => command.run(),
        }
    }
}
